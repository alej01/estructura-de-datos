/**********************************************************************
    Instituto TecnolÃ³gico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Ejemplos PrÃ¡cticos: Lista de Estudiantes
    Estudiante: Alejandro Duran/2019024258
**********************************************************************/
//DefiniciÃ³n de Macros
#define LONGITUD_MAXIMA_NOMBRE 50 
#define LONGITUD_MAXIMA_CARNET 12

//DefiniciÃ³n de Estructuras
typedef struct nodo_estudiante
{
	int carnet;
	char *nombre;

	struct nodo_estudiante *ref_siguiente;
}nodo_estudiante;

typedef struct lista_estudiantes
{
	nodo_estudiante *ref_inicio;
	int cantidad;
}lista_estudiantes;

//DefiniciÃ³n de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe parÃ¡metros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento: 
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_estudiante* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: No recibe parametros.
	Salidas: No retorna nada.
	Funcionamiento: 
    * Inicializa la lista de estudiantes al reservar espacios de memoria
    * para ella asi como definir su cantidad.
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: Estudiante tipo nodo_estduiante
	Salidas: No tiene.
	Funcionamiento: 
    * Crea un nuevo estudiante
    * Inicializa la lista
    * Apunta el siguiente de la lista a nulo y el inicio a el nuevo
    * estudiante.
-----------------------------------------------------------------------*/
void insertar_inicio(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	insertar_final
	Entradas: nuevo estudiante tipo nodo_estudiante.
	Salidas: No tiene.
	Funcionamiento: Verifica si la lista esta vacia, o si es el primer 
    elemento que es ingresado, si no lo es, navega la lista hasta que 
    el siguiente elemento sea NULL y cuando lo es, inserta el elemento
    en la siguiente posicion, y suma +1 al contador.
-----------------------------------------------------------------------*/
void insertar_final(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: Un indice tipo int
	Salidas: No tiene.
	Funcionamiento: Toma un indice, verifica si es 0, si lo es libera
    la primera posicion, de lo contrario recorre la lista con el temporal
    hasta encontrar el indice y remplaza por el siguiente
-----------------------------------------------------------------------*/
void borrar_por_indice(int indice);
 /*-----------------------------------------------------------------------
	buscar_por_indice
	Entradas: indice tipo int
	Salidas: Nodo temporal tipo nodo_estudiante
	Funcionamiento: Busca por indice un nodo y lo retorna.
-----------------------------------------------------------------------*/
nodo_estudiante* buscar_por_indice(int indice);

 /*-----------------------------------------------------------------------
	validar_carnets
	Entradas: carnet_almacenado tipo int, carnet_ingresado tipo int
	Salidas: No tiene.
	Funcionamiento: verifica si el carnet ingresado es igual al registrado.
-----------------------------------------------------------------------*/
void validar_carnets(int carnet_almacenado, int carnet_ingresado);
 /*-----------------------------------------------------------------------
	menu
	Entradas: No tiene.
	Salidas: No tiene.
	Funcionamiento: Contiene el switch con las opciones y operaciones a 
    realizar.
-----------------------------------------------------------------------*/
void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas: No tiene.
	Salidas: 0
	Funcionamiento: imprime el menu
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: max_size tipo size_t
	Salidas: buffer
	Funcionamiento: Crea un buffer
-----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
	Entradas: max_size tipo size_t
	Salidas: numerical_input
	Funcionamiento: Se utiliza para obtener el input del usuario
-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);
