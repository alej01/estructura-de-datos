from Nodo import Nodo

class lista_enlazada:

    def __init__(self):
        self.cabeza = None

    def agregar_inicio(self,valor):
        temp = Nodo(valor)
        temp.siguiente = self.cabeza
        self.cabeza = temp
        del temp

    def traverse_list(self):
     if self.cabeza is None:
        print("Lista estÃ¡ vacia")
        return
     else:
        n = self.cabeza
        while n is not None:
            print(n.valor, " ")
            n = n.siguiente

    def agregar_final(self, valor):

      indice = self.cabeza
      while indice.siguiente != None:
       indice = indice.siguiente
       temp = Nodo(valor)
       indice.siguiente = temp
       self.longitud += 1

    def limpiar_lista(self):

     indice = self.cabeza
     while indice != None:
      temp = indice
      indice = indice.siguiente
      temp.siguiente = None
      del temp
      self.cabeza = None
      self.longitud = 0

    def buscar_ultimo(self):

     indice = self.cabeza
     while indice.siguiente != None:
      indice = indice.siguiente
      return indice

    def invertir(self):

     if self.cabeza == None:
      print("No hay elementos en la lista")

     else:
      indice = self.cabeza
      anterior = None
      while indice != None:
       sig = indice.siguiente
       indice.siguiente = anterior
       anterior = indice
       indice = sig
       self.cabeza = anterior

    def buscar_indice(self, indice_buscado, indice=0):
        reco = self.cabeza
        while reco != None:
            if indice == indice_buscado:
                print("El valor en indice", indice, "es: ", reco.valor)
            reco = reco.siguiente
            indice += 1
        return "No se encuentra ese indice"


miLista = lista_enlazada()
miLista.agregar_inicio(1)
miLista.agregar_inicio(2)
miLista.agregar_inicio(3)
miLista.agregar_inicio(4)
miLista.agregar_inicio(5)
miLista.agregar_inicio(6)
miLista.agregar_inicio(7)
miLista.traverse_list()

miLista.buscar_indice(2)
miLista.buscar_indice(3)

miLista.traverse_list()
miLista.invertir()
miLista.traverse_list()

miLista.buscar_indice(2)
miLista.buscar_indice(3)

miLista.limpiar_lista()
miLista.traverse_list()
miLista.invertir()
