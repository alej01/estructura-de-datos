char *arregloFamilia[]= {"Espada","Corazon","Diamante","Trebol"}; // arreglo para definir las familias de cada carta
//este arreglo puede ser opcional pero por cuestiones de debuggeo, es bastante util.

int arregloValor[] = {0,13,26,39}; //arreglo usado para los valores de la familia y el valor total
char *arregloTipo[15] = {"A","2","3","4","5","6","7","8","9","â¿","J","Q","K"}; //los nombres posibles que tendra cada carta
int ValoresTotales[52]; //el arreglo que es utilizado para colocar los valores totales para luego ordenarlos y crear una nueva baraja

//definimos los datos y punteros que tendra cada carta
typedef struct carta {
    struct carta* cartaSiguiente;
    struct carta* cartaAnterior;
    int valorFamilia;
    int valorTotal;
    char familia[11];
    char nombreCarta[2];
}carta;

//definimos lso punteros que tendra la baraja para su manipulacion
typedef struct baraja {
        carta * cartaInicial;
        carta * cartaFinal;
}baraja;
