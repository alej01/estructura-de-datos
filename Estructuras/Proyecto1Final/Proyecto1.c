#include <stdio.h>
#include <stdlib.h>
#include <string.h> //Biblioteca para implementar algunos strings en cartas
#include <time.h>  // Biblioteca utilizada para la implementacion de numeros aleatorios (la semilla)
#include "DatosDeCartas.h" //Archivos encabezado utilizados para funcionalidad de programa y evitar la saturacion de codigo
#include "Quicksort.h"
/*Color schemes*/
#define Rojo "\033[1;31m"
#define Azul "\033[0;34m"
#define Amarillo "\033[0;33m"
#define EndColor "\033[0m"
// Variable global utilizada para manipular la baraja
// En ciertas funcionalidades como la de ordenar esta se desconstruye y vuelve
// a iniciarse
baraja *punteroBaraja = NULL;


// ***FUNCIONES PARA INICIALIZAR LA BARAJA CON SUS CARTAS *** //

baraja* iniciarBaraja(baraja *nuevaBaraja){

// Alocar memoria para las barajas que se crean y sus respectivos punteros
    nuevaBaraja = malloc(sizeof(baraja));
    nuevaBaraja->cartaInicial= NULL;
    nuevaBaraja->cartaFinal=NULL;
    return nuevaBaraja;
}

carta* crearCarta(){

//colocar en memoria una carta de modo que sus datos y punteros se asignen a valores predeterminados
//NULL para los punteros, 0 para los valores y un string vacio para los nombres
    carta* temporal;
    temporal = malloc(sizeof(carta));
    temporal->cartaSiguiente = NULL;
    temporal->cartaAnterior = NULL;
    temporal->valorFamilia = 0;
    temporal->valorTotal = 0;
    strcpy(temporal->nombreCarta,"");
    strcpy(temporal->familia,"");
    return temporal;
}

void llenarBaraja(){

//Funcion que coloca los datos correspondientes a cada carta de la baraja en orden
    for (int indiceFamilia = 0; indiceFamilia<4; indiceFamilia++){
    //Primer recorrido sirve para colocar los valores de familia
        for (int indiceCarta =0; indiceCarta<13; indiceCarta++){
        //Segundo recorrido se utiliza para colocar el valor total de cada carta en una familia en especifico
            carta *cartaAgregada = crearCarta();
            if(punteroBaraja == NULL){

                punteroBaraja = iniciarBaraja(punteroBaraja);
                cartaAgregada->cartaAnterior=NULL;
                cartaAgregada->cartaSiguiente=NULL;

                cartaAgregada->valorFamilia=arregloValor[indiceFamilia];
                cartaAgregada->valorTotal= cartaAgregada->valorFamilia + indiceCarta+1 ;

                strcpy (cartaAgregada->familia,arregloFamilia[indiceFamilia]);
                strcpy (cartaAgregada->nombreCarta,arregloTipo[indiceCarta]);

                punteroBaraja->cartaFinal = cartaAgregada;
                punteroBaraja->cartaInicial = cartaAgregada;

            }else{

            cartaAgregada->cartaAnterior = punteroBaraja->cartaFinal;
            punteroBaraja->cartaFinal->cartaSiguiente = cartaAgregada;
            cartaAgregada->cartaSiguiente= NULL;

            cartaAgregada->valorFamilia=arregloValor[indiceFamilia];
            cartaAgregada->valorTotal= cartaAgregada->valorFamilia + indiceCarta+1 ;

            strcpy (cartaAgregada->familia, arregloFamilia[indiceFamilia]);
            strcpy (cartaAgregada->nombreCarta,arregloTipo[indiceCarta]);

            punteroBaraja->cartaFinal = cartaAgregada;

            }

        }
    }
}

// *** FUNCIONES PARA LAS OCPCIONES DE IMPRIMIR LAS CARTAS *** //


void imprimirCarta(carta *cartaImprimir){
    //Se extrae el valor familia de cada carta para comparar
    int valorFamilia = cartaImprimir->valorFamilia;
    char*simbolo;
    char *color;  
    if  (valorFamilia == 26){
        simbolo = " â¦ ";
        color = Rojo;
        }
        
    if(valorFamilia == 13){
        simbolo = " â¥ ";
        color = Rojo;
        }

    if(valorFamilia == 0){
        simbolo = " â  ";
        color = Azul;
        
    }
    if(valorFamilia == 39){
        
        simbolo = " â£ ";
        color = Azul;
        
        
        }
        
        printf("    ââââââââââââ\n");
        printf("    â");printf("%s",color);printf("%s ",cartaImprimir->nombreCarta); printf(EndColor); printf("        â\n");
        printf("    â          â\n");
        printf("    â          â\n");
        printf("    â   "); printf("%s",color); printf("%s",simbolo); printf(EndColor); printf("    â\n");
        printf("    â          â\n");
        printf("    â          â\n");
        printf("    â");  printf("%s",color);printf("        %s ",cartaImprimir->nombreCarta); printf(EndColor);printf("â\n");
        printf(EndColor);
        printf("    ââââââââââââ\n");
        printf(EndColor);
        
        }

void mostrarBaraja(){
    //Se crea un puntero que vaya recorriendo la lista de cartas y las imprima con ayuda de la funcion imprimirCarta();
    carta *cartaIndice = punteroBaraja->cartaInicial;
    int contador = 0;
    while(contador <52){
    imprimirCarta(cartaIndice);
    cartaIndice = cartaIndice->cartaSiguiente;
    ++contador;
    }
}
carta* obtenerCartaPorIndice(int indiceDeseado){
    //Se crea un puntero que vaya recorriendo la lista y vaya retornando el numero de indice de cada carta.
    int indice =0;
    carta* cartaIndice = punteroBaraja->cartaInicial;
    while(indice<indiceDeseado){
    cartaIndice= cartaIndice->cartaSiguiente;
    indice++;
    }
    return cartaIndice;
}

// ***FUNCIONES RELACIONADAS CON LA OPCION DE MEZCLAR (BARAJAR) LAS CARTAS*** //

int* generarNumero(){
    //funcion que se encarga de generar un numero aleatorio
    static int numerosAleatorios[20];
    srand((unsigned)time(NULL));
    for(int i = 0; i<20; i++){
        numerosAleatorios[i] = 1+rand()%50;
    }
    return numerosAleatorios;
}


void barajarPrimeraCarta (){
    //Se encarga de buscar una carta aleatoria y colocarla al inicio de lista recursivamente, barajando asi las cartas.
    int  cantidadParticiones = 0;
    int *indicesAleatorios = generarNumero();
    while (cantidadParticiones< 20){
        int indice = 0;
        carta *cartaIndice = punteroBaraja->cartaInicial;
        while(indice <  (indicesAleatorios[cantidadParticiones])){
            cartaIndice = cartaIndice->cartaSiguiente;
            indice++;
            }
        carta *tempAnterior = cartaIndice->cartaAnterior;
        carta *tempSiguiente = cartaIndice->cartaSiguiente;
        cartaIndice ->cartaAnterior = NULL;
        cartaIndice->cartaSiguiente = punteroBaraja->cartaInicial;
        punteroBaraja->cartaInicial->cartaAnterior= cartaIndice;
        tempAnterior->cartaSiguiente= tempSiguiente;
        tempSiguiente->cartaAnterior=tempAnterior;
        punteroBaraja->cartaInicial=cartaIndice;
        cantidadParticiones ++;
    }
}

// *** FUNCIONES ASIGNADAS A LA OCPION DE ORDENAR CARTAS *** //

void obtenerValoresTotales(){
    //Funcion que se encarga de obtener el valor total de cada carta e introducirlos en un arreglo.
    carta* punteroIndice;
    int indiceArregloValorTotales = 0;
    for (punteroIndice= punteroBaraja->cartaInicial; punteroIndice != NULL; punteroIndice = punteroIndice->cartaSiguiente){
        int valorTotalActual = punteroIndice->valorTotal;
        ValoresTotales[indiceArregloValorTotales]= valorTotalActual;
        //printf("valor total %d es: %d \n",indiceArregloValorTotales,valorTotalActual);
        indiceArregloValorTotales++;
    }

}
carta* buscarCartaPorValorTotal(int valorTotal){
    //Busca el valor total de cada carta mediante un puntero, que funciona como un indice.
    carta* cartaIndice = punteroBaraja->cartaInicial;
    while(cartaIndice->valorTotal != valorTotal){
        cartaIndice = cartaIndice->cartaSiguiente;
    }
    return cartaIndice;
}

//Se encarga de extraer la carta de la baraja, para luego ordenarla en una lista nueva.
carta *extraerCarta(carta* cartaParaExtraer){
    //caso para desconectar la primer y ultima carta
    if(cartaParaExtraer->cartaSiguiente == NULL && cartaParaExtraer->cartaAnterior == NULL){
        punteroBaraja->cartaFinal = NULL;
        punteroBaraja->cartaInicial = NULL;
    }else{ //Caso para desconectar la carta de la siguiente en la lista, se crea un temporal, para no perderla.
        if(cartaParaExtraer->cartaSiguiente == NULL){
            carta* tempAnterior = cartaParaExtraer->cartaAnterior;
            tempAnterior->cartaSiguiente = NULL;
            cartaParaExtraer->cartaAnterior = NULL;
            punteroBaraja->cartaFinal = tempAnterior;
        }//Caso para desconectar la carta de la anterior en la lista, se crea un temporal, para no perderla.
        if(cartaParaExtraer->cartaAnterior == NULL){
            carta* tempSiguiente = cartaParaExtraer->cartaSiguiente;
            cartaParaExtraer->cartaSiguiente = NULL;
            tempSiguiente->cartaAnterior = NULL;
            punteroBaraja->cartaInicial = tempSiguiente;
        }else{

            carta* tempAnterior = cartaParaExtraer->cartaAnterior;
            carta* tempSiguiente = cartaParaExtraer->cartaSiguiente;
            cartaParaExtraer->cartaAnterior =NULL;  cartaParaExtraer->cartaSiguiente = NULL;
            tempAnterior->cartaSiguiente = tempSiguiente;
            tempSiguiente->cartaAnterior = tempAnterior;
        }
    }return cartaParaExtraer;
}

void construirBarajaPorValorTotal(int ValoresTotalesOrdenados[]){
    //Crea la nueva lista, se va ordenando conforme a su valor total.
    int indiceValoresTotales = 0;
    int valorTotalActual;
    baraja* NuevaBaraja = NULL;
    NuevaBaraja = iniciarBaraja(NuevaBaraja);
    carta* cartaIndice = NuevaBaraja->cartaInicial;
    carta* cartaAgregada =NULL;
    while (indiceValoresTotales < 52){
        valorTotalActual = ValoresTotalesOrdenados[indiceValoresTotales];
        cartaAgregada = buscarCartaPorValorTotal(valorTotalActual);
        cartaAgregada = extraerCarta(cartaAgregada);
        //inicializa la nueva lista.
        if(cartaIndice == NULL){
            NuevaBaraja->cartaInicial = cartaAgregada;
            NuevaBaraja->cartaFinal = cartaAgregada;
            cartaAgregada ->cartaSiguiente = NULL;
            cartaAgregada->cartaAnterior = NULL;
            cartaIndice = cartaAgregada;
        //Va agregando cada carta de forma ordenada.
        }else{
           cartaIndice->cartaSiguiente = cartaAgregada;
           cartaAgregada->cartaAnterior = NuevaBaraja->cartaFinal;
           cartaAgregada->cartaSiguiente = NULL;
           NuevaBaraja->cartaFinal = cartaAgregada;
           cartaIndice = cartaIndice->cartaSiguiente;
        }
        indiceValoresTotales ++;
  }
    punteroBaraja = NuevaBaraja;
    punteroBaraja->cartaInicial = NuevaBaraja->cartaInicial;
    punteroBaraja->cartaFinal = NuevaBaraja->cartaFinal;
}


// *** FUNCIONES PARA QUE EL USUARIO INTERACTUE CON EL PROGRAMA *** ///
void menu(){

    int opcion;
    int indiceActual = 0;

do{
    printf(Amarillo);
    printf("|=========================================|\n");
    printf("|----------------Bienvenido---------------|\n");
    printf("|-----------------------------------------|\n");
    printf("|--------------CASINO ROYALE--------------|\n");
    printf("|-----------------------------------------|\n");
    printf("|-Seleccione la accion que desea realizar-|\n");
    printf("|-----------------------------------------|\n");
	printf("|---------1. Mostrar carta actual---------|\n");
	printf("|-------2. Mostrar siguiente carta--------|\n");
	printf("|--------3. Mostrar carta anterior--------|\n");
	printf("|--------4. Mostrar toda la baraja--------|\n");
	printf("|---------------5. Ordenar----------------|\n");
	printf("|---------------6. Barajar----------------|\n");
	printf("|---------------7. Salir------------------|\n");
    printf("|=========================================|\n");
    printf(EndColor);
    carta *ref_cartaActual = punteroBaraja->cartaInicial;
    scanf("%d", &opcion);
    switch (opcion){
    case 1: //imprime la carta que esta al inicio de la baraja, con ayuda de imprimirBaraja()
        ref_cartaActual= obtenerCartaPorIndice(indiceActual);
        imprimirCarta(ref_cartaActual);
        break;
    case 2://imprime la carta siguiente de la actual, con ayuda de imprimirBaraja()
        if (indiceActual == 51){
        printf("Ha llegado al final de la baraja \n");
        }else{
        indiceActual++;
        ref_cartaActual = obtenerCartaPorIndice(indiceActual);
        imprimirCarta(ref_cartaActual);
        }break;
    case 3://imprime la carta anterior de la actual, con ayuda de imprimirBaraja()
        if (indiceActual == 0){
        printf("Su carta actual es la primera de la baraja \n");
        }else{
        indiceActual--;
        ref_cartaActual = obtenerCartaPorIndice(indiceActual);
        imprimirCarta(ref_cartaActual);
        }break;
    case 4: //muestra la baraja.
        mostrarBaraja();
        break;
    case 5: //ordena la baraja.
        obtenerValoresTotales();
        quick_sort(ValoresTotales, 0, 51);
        construirBarajaPorValorTotal(ValoresTotales);
        break;
    case 6: //desordena la bajara, 20 veces en un intento.
        for (int barajadas = 0; barajadas <20; barajadas++){
            barajarPrimeraCarta();
        }
        break;

    default:
        printf(Rojo);
        printf("         Ingrese una opcion valida! \n");
        printf(EndColor);
        break;
    }
  }while(opcion != 7);
}


int main(){

    llenarBaraja();
    menu();
    return 0;
}
