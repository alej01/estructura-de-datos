/*Funcion para hacer el swap
aquÃ­ irÃ­an los punteros de las
cartas para hacer el swap con el temp*/
void swap(int *a, int *b)

{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}


/*La particion se utiliza para retornar el valor del numero exacto
  de la posicion del pivote cuando se acomoda en el sort la variable,
  low se utiliza para apuntar a la posicion del siguiente elemento mas bajo*/

int particion (int ValoresTotalesDesordenados[], int first, int last)
{
    int pivote = ValoresTotalesDesordenados[last]; // Cambia el pivote
    int low = first;
    int i = first;
    while(i <= last-1 ){// en vez del while se puede utilizar = for(i=first;i<last;i++)
        if(ValoresTotalesDesordenados[i] < pivote){
            swap(&ValoresTotalesDesordenados[i], &ValoresTotalesDesordenados[low]);
            low++;
        }
        i++;
    }
    swap(&ValoresTotalesDesordenados[last], &ValoresTotalesDesordenados[low]);
    // depues de poner todos los elementos menores al pivote
    // se cambia el pivote a su posicion original y se retorna su valor

    return low;
}

void quick_sort(int ValoresTotalesDesordenados[], int first, int last)
{
    int pivote_pos;
    if(first < last){
        pivote_pos = particion(ValoresTotalesDesordenados, first, last);
        quick_sort(ValoresTotalesDesordenados, first, pivote_pos-1);
        quick_sort(ValoresTotalesDesordenados, pivote_pos+1, last);
    }
}
