#include <stdio.h> 
#include <stdlib.h> 
  

typedef struct node 
{ 
     int data; 
     struct node* left; 
     struct node* right; 
}nodo; 
  

struct nodo* newNode(int data) 
{ 
     nodo* nodo = (struct node*) 
                                  malloc(sizeof(struct node)); 
     nodo->data = data; 
     nodo->left = NULL; 
     nodo->right = NULL; 
  
     return(nodo); 
} 
  

void printPostorder(nodo* node) 
{ 
     if (node == NULL) 
        return; 

     printPostorder(node->left); 
  

     printPostorder(node->right); 
  

     printf("%d ", node->data); 
} 
  

void printInorder(nodo* node) 
{ 
     if (node == NULL) 
          return; 
  

     printInorder(node->left); 
  

     printf("%d ", node->data);   


     printInorder(node->right); 
} 

int main() 
{ 
     nodo *root  = newNode(7); 
     root->left             = newNode(6); 
     root->right           = newNode(0); 
     root->left->left     = newNode(4); 
     root->left->right   = newNode(8);  
  
     printf("\nPostorder traversal of binary tree is \n"); 
     printPostorder(root); 
  
     getchar(); 
     return 0; 
} 
