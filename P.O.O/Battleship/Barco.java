
package battleship;


public class Barco {
    private final String nombre;
    private final int tamano;
    private int vidas;

    public Barco(String nombre, int tamano) {
        this.nombre = nombre;
        this.tamano = tamano;
        this.vidas = tamano;
    }
    //Funcion para determinar si un barco fue herido//
    public void atinado() {
        if(vidas > 0) {
            System.out.printf("%nBuen Tiro! El barco" + nombre + " fue herido.");
            vidas--;
        } else {
            System.out.println("Barco Destruido");
        }
    }
    //Funcion para determinar el estado del barco//
    public Resultado getState() {
        if(vidas == 0) {
            return Resultado.DESTRUIDO;
        } else if(vidas < tamano) {
            return Resultado.TIRO_CERCANO;
        } else {
            return Resultado.Tiro_ERRADO;
        }
    }
    //Get nombre del barco//
    public String getName() {
        return nombre;
    }
    //Get tamano del barco//
    public int getSize() {
        return tamano;
    }
}
