
package battleship;

import java.awt.*;
import java.util.Scanner;


public class JugadorHumano implements InterfazJugador {
    private int VidasRestantes = 17;

    private final int id;
    private final Tablero Tablero;
    private final Scanner scanner;

    public JugadorHumano(int id) {
        this.id = id;
        this.Tablero = new Tablero();
        this.scanner = new Scanner(System.in);
    }
    /*oBTENER id*/
    public int getId() {
        return id;
    }
     /*obtener el tablero*/
    public Tablero getTablero() {
        return Tablero;
    }
     /*Prompt para ingresar barcos*/
    @Override
    public void colocarBarcos() {
        System.out.printf("%n======== Jugador" + id + " - coloque sus barcos ========%n");
        Tablero.ColocarBarcosTablero();
    }
     /*Se valida la posicion de un disparo al pedir las coordenadas*/
    @Override
    public void DispararA(InterfazJugador oponente) {
        System.out.printf("%n Jugador" + id + " - Ingrese las coordenadas de su ataque: ");

        boolean ElPuntpoEsValido = false;
        while(!ElPuntpoEsValido) {
            try {
                Point point = new Point(scanner.nextInt(), scanner.nextInt());
                int x = (int)point.getX() - 1;
                int y = (int)point.getY() - 1;

                Resultado resultado = ((JugadorHumano)oponente)
                        .getTablero()
                        .getPosicion(x, y)
                        .DispararA();

                if(resultado == Resultado.TIRO_CERCANO ||  resultado == Resultado.DESTRUIDO) {
                    VidasRestantes--;
                }

                ElPuntpoEsValido = true;
            } catch(IllegalArgumentException e) {
                System.out.printf(e.getMessage());
            }
        }
    }
     /*Get el numero de vidas*/
    @Override
    public int getVidasTotales() {
        return VidasRestantes;
    }


 
}
