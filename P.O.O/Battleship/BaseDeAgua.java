package battleship;


public class BaseDeAgua implements InterfazJuego {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static String p = ANSI_CYAN + "~" + ANSI_RESET;
    private boolean isThisFieldHit = false;

    @Override
    public String getIcono() {
        return isThisFieldHit ? "" : p;
    }

    @Override
    public Resultado DispararA() {
        isThisFieldHit = true;
        return Resultado.Tiro_ERRADO;
    }
}
