package battleship;

public class Juego {
    private JugadorHumano[] jugadores;
    
    /*Se crean dos jugadores*/
    public Juego() {

        this.jugadores = new JugadorHumano[]{
                new JugadorHumano(1),
                new JugadorHumano(2)
        };
    }

    public void ini() {
        int i = 0;
        int j = 1;
        int len = jugadores.length;
        JugadorHumano player = null;

        this.jugadores[i].colocarBarcos();
        this.jugadores[j].colocarBarcos();

        while(jugadores[0].getVidasTotales()> 0 && jugadores[1].getVidasTotales()> 0) {

            jugadores[i++ % len].DispararA(jugadores[j++ % len]);
            player = (jugadores[0].getVidasTotales()< jugadores[1].getVidasTotales()) ?
                    jugadores[1] :
                    jugadores[0];
        }

        System.out.printf("Congrats Player %d, you won!",player.getId());
    }
}