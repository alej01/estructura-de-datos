
package battleship;

public interface InterfazJugador {
    
    void colocarBarcos();
    void DispararA(InterfazJugador opponent);
    int getVidasTotales();
}