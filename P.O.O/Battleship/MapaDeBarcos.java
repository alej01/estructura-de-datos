package battleship;

public class MapaDeBarcos implements InterfazJuego {
    private final Barco barco;

    public MapaDeBarcos(Barco barco) {
        this.barco = barco;
    }

    @Override
    public String getIcono() {
        String icono;
        Resultado EstadoDeBarco = barco.getState();
        switch (EstadoDeBarco) {
            case TIRO_CERCANO: icono = "O";
                break;
            case DESTRUIDO: icono = "O";
                break;
            case Tiro_ERRADO: icono = "X";
                break;
            default: icono = " ";
                break;
        }
        return icono;
    }

    @Override
    public Resultado DispararA() {
        barco.atinado();
        return barco.getState();
    }
}