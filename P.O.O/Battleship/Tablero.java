package battleship;


import java.awt.Point;
import java.io.IOException;
import java.util.Scanner;


public class Tablero {
 
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static String p = ANSI_CYAN + "~" + ANSI_RESET;
    private static final  String WATER = p;
    private static final int tamanoTablero = 10;
    private static final char[] Letras_Tablero = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
    private static final String HORIZONTAL = "H";
    private static final String VERTICAL = "V";

    private final Scanner scanner;
    private final InterfazJuego[][] NuevoTablero;
    private static final Barco[] BARCOS;
    static {
        BARCOS = new Barco[] {
            
                new Barco("PortaAviones", TamanoDelBarco.PORTAAVIONES),
                new Barco("Submarino", TamanoDelBarco.SUBMARINO),
                new Barco("Submarino", TamanoDelBarco.SUBMARINO2),
                new Barco("Submarino", TamanoDelBarco.SUBMARINO3),
                new Barco("Destrcutor", TamanoDelBarco.DESTRUCTOR),
                new Barco("Destrcutor", TamanoDelBarco.DESTRUCTOR2),
                new Barco("Destrcutor", TamanoDelBarco.DESTRUCTOR3),
                new Barco("Fragata", TamanoDelBarco.FRAGATA),
                new Barco("Fragata", TamanoDelBarco.FRAGATA1),               
        };
    }

    public Tablero() {
        
        this.scanner = new Scanner(System.in);
        this.NuevoTablero = new InterfazJuego[tamanoTablero][tamanoTablero];
        for(int i = 0; i < tamanoTablero; i++) {
            for(int j = 0; j < tamanoTablero; j++) {
                NuevoTablero[i][j] = new BaseDeAgua();
            }
        }

    }
    /*Coloca los barcos en el tablero con su punto de inicio y direccion*/
    public void ColocarBarcosTablero(){
            imprimirTablero();
        for(Barco barco : BARCOS) {
            boolean Direccion = DireccionDeBarco();
            Point puntoInicio = pedirCoordenadas(barco, Direccion);
            colocarBarcoPosicionValida(barco, puntoInicio, Direccion);

            imprimirTablero();
            
        }     
         System.out.print("\033[H\033[2J");  
         System.out.flush();
    }
      /*get una posicon en el tablero*/
    public InterfazJuego getPosicion(int x, int y) {
        if(!EstaEnTablero(x, y)) {
            throw new IllegalArgumentException("Fuera del tablero - ingrese una coordenada valida: ");
        }
        return NuevoTablero[y][x];
    }
      /*impresion de tablero*/
    public void imprimirTablero() {
        System.out.print("\t");

        for(int i = 0; i < tamanoTablero; i++) {
            System.out.print(Letras_Tablero[i] + "\t");
        }

        System.out.println();

        for(int i = 0; i < tamanoTablero; i++) {
            System.out.print((i+1) + "\t");
            for(int j = 0; j < tamanoTablero; j++) {
                System.out.print(NuevoTablero[i][j].getIcono() + "\t");
            }
            System.out.println();
        }
    }
    /*Toma la desicion de horizomtal o vertical y direcciona el barco con
     con las coordenadas*/
        private boolean DireccionDeBarco() {
        System.out.printf("%nDesea colocar su barco de manera vertical u horizontal? (V)(H)?");
        String direccion;
        do {
            direccion = scanner.nextLine().trim();
        }while (!HORIZONTAL.equals(direccion) && !VERTICAL.equals(direccion));

        return HORIZONTAL.equals(direccion);
    }
      /*Se piden las coordenadas mientras el punto sea valido*/
        private Point pedirCoordenadas(Barco barco, boolean posicion) {
        
        Point coordenada;
        do {
            System.out.printf("%nIngrese la posicion del navio " + barco.getName()+" tamaño "+barco.getSize());
            System.out.printf("%nIngrese la coordenada en X:");
            int x = scanner.nextInt();
            System.out.printf("%nIngrese la coordenada en Y:");
            int y = scanner.nextInt();
            coordenada = new Point(x,y);
        } while(!elPuntoEsValido(coordenada, barco.getSize(), posicion));

        return coordenada;
    }
      /*Validacion del punto*/
    private boolean elPuntoEsValido(Point coordenada, int len, boolean posicion) {
        int xDiff = 0;
        int yDiff = 0;
        if(posicion) {
            xDiff = 1;
        } else {
            yDiff = 1;
        }

        int x = (int)coordenada.getX() - 1;
        int y = (int)coordenada.getY() - 1;
        if(!EstaEnTablero(x, y) ||
                (!EstaEnTablero(x + len,y) && posicion) ||
                (!EstaEnTablero(x, y + len) && !posicion)
                ) {
            return false;
        }

        for(int i = 0; i < len; i++) {
            if(NuevoTablero[(int)coordenada.getY() + i *yDiff - 1]
                    [(int)coordenada.getX() + i *xDiff - 1].getIcono() == null ? WATER != null : !NuevoTablero[(int)coordenada.getY() + i *yDiff - 1]
                            [(int)coordenada.getX() + i *xDiff - 1].getIcono().equals(WATER)){
                return false;
            }
        }
        return true;
    }
      /*Colocar barco en una posicion validada*/
    private void colocarBarcoPosicionValida (Barco barco, Point PuntoInicio, boolean Direccion) {
        int xDiff = 0;
        int yDiff = 0;
        if(Direccion) {
            xDiff = 1;
        } else {
            yDiff = 1;
        }
        for(int i = 0; i < barco.getSize() ; i++) {
            NuevoTablero[(int)PuntoInicio.getY() + i*yDiff - 1]
                    [(int)PuntoInicio.getX()+ i*xDiff - 1] = new MapaDeBarcos(barco);
        }
    }
    /*Determina si las coordenadas estan dentro del tablero*/
    private boolean EstaEnTablero(int x, int y){
        return x <= tamanoTablero && x >= 0
                && y <= tamanoTablero && y >= 0;
    }
}