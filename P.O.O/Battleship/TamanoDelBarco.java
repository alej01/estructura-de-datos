package battleship;

public class TamanoDelBarco {
    private TamanoDelBarco() {
    }

    public static final int PORTAAVIONES = 4;
    public static final int SUBMARINO = 3;
    public static final int SUBMARINO2 = 3;
    public static final int SUBMARINO3 = 3;
    public static final int DESTRUCTOR = 2;
    public static final int DESTRUCTOR2 = 2;
    public static final int DESTRUCTOR3 = 2;
    public static final int FRAGATA = 1;
    public static final int FRAGATA1 = 1;
}