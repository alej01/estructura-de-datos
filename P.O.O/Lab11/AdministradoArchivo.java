/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SuperMercado;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 *
 * @author Alejandro Duran
 */
public class AdministradoArchivo {
    
    private String nombre;
    private float precioSinImpuesto;
    private float porcentajeImpuesto;
    int x = 20,y = 20;
    String[][] matrix = new String[x][y];

    public AdministradoArchivo() {
    }
    
    public void LeerProductos(SuperMercado.Supermercado supermarket) throws FileNotFoundException, IOException{
    
    File excelFile = new File("C://Users/Alejandro Duran//Documents//U//productos.xlsx/");
    FileInputStream fis = new FileInputStream(excelFile);

    // we create an XSSF Workbook object for our XLSX Excel File
    XSSFWorkbook workbook = new XSSFWorkbook(fis);
    // we get first sheet
    XSSFSheet sheet = workbook.getSheetAt(0); 

    // we iterate on rows
    Iterator<Row> rowIt = sheet.iterator();

    while(rowIt.hasNext()) {
      Row row = rowIt.next();

      // iterate on cells for the current row
      Iterator<Cell> cellIterator = row.cellIterator();

      while (cellIterator.hasNext()) {
        Cell cell = cellIterator.next();
        if(cell == null){break;}
        matrix[row.getRowNum()][cell.getColumnIndex()] = cell.toString();     
                
                }
      }
    workbook.close();
    fis.close();
    
    for (int i = 1; i < matrix.length; i++) {
        for (int j = 0; j < 1; j++) {
            if(matrix[i][j]==null){break;}
            this.nombre = matrix[i][j];
            this.precioSinImpuesto = Float.parseFloat(matrix[i][1]);
            this.porcentajeImpuesto =  Float.parseFloat(matrix[i][2]);
            if(matrix[i][3].equals("UNIDAD")){
             SuperMercado.Producto productU = new Producto(nombre, precioSinImpuesto, porcentajeImpuesto, Producto.tipoVenta.POR_UNIDAD);
             supermarket.agregarProductoInventarioVentaUnitaria(nombre, precioSinImpuesto, porcentajeImpuesto);          
            }else{
             SuperMercado.Producto productP = new Producto(nombre, precioSinImpuesto, porcentajeImpuesto, Producto.tipoVenta.POR_PESO);  
             supermarket.agregarProductoInventarioVentaPorPeso(nombre, precioSinImpuesto, porcentajeImpuesto);
            }
           
    }
    
}
    
}            

}

    