package SuperMercado;

import java.io.IOException;



/**
 *
 * @author Alejandro Duran
 */
public class Main {
    
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        Menu menu = new Menu();
        menu.menu();
  }
        
        
}
    