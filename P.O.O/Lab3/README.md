C-2101 Programación Orientada a Objetos
Prof: MSc. Samanta Ramijan Carmiol
Laboratorio 3
Herencia y Polimorfismo

Defina una nueva clase de PilaTEC que extienda a ArrayList y dibuje el diagrama
UML para las clases.
Implemente la solución en código.
Escriba además un programa de prueba que le solicite al usuario 5 enteros y
después los muestre del último al primero.

Aspectos Administrativos

Límite para la entrega de la asignación: Martes 3 de septiembre a las 3pm.
Plataforma de revisión: Repositorio de código git
Cada archivo debe estar debidamente documentado con la información personal del
estudiante que lo escriba, además de explicar su código e indicar cualquier
referencia a código de terceros
Se debe incluir un archivo README que contenga el enunciado de los ejercicios.