//Alejandro Duran B.
//2019024258

package laboratorio1;
import java.util.Date;
        
        
public class Cuenta {
    
    private int id;
    private double balance;
    private double tasaDeInteresAnual;
    private Date fechaCreacion;
    
    public Cuenta(int id, double balance, double tasaDeInteresAnual) {
        this.id = id;
        this.balance = balance;
        this.tasaDeInteresAnual = tasaDeInteresAnual;
  
    }

    public Cuenta(int id, double balance) {
        this.id = id;
        this.balance = balance;
    }
    
     public Cuenta(int id) {
        this.id = id;
        this.balance = 100.000;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getTasaDeInteresAnual() {
        return tasaDeInteresAnual;
    }

    public void setTasaDeInteresAnual(double tasaDeInteresAnual) {
        this.tasaDeInteresAnual = tasaDeInteresAnual;
    }

    public Date getFechaCreacion() {
        
        Date Mydate = new Date();
        return Mydate;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    public double obtenerTasaDeInteresMensual( ){
        return this.tasaDeInteresAnual/12;
}

    public double calcularInteresMensual(){
        
       double interesMensual;
       return interesMensual = balance*tasaDeInteresAnual;
    }
    
    public void retirarDinero(double monto){
        
        balance = balance-monto;
    
    }
    public void depositarDinero(double cantidad){
        
        balance= balance + cantidad;
    }
}


