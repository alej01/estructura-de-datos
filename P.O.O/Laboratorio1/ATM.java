//Alejandro Duran B.
//2019024258

package laboratorio1;

import java.util.Scanner;

public class ATM {

    
    public static void main(String[] args) {
        
        Cuenta[] cuentas = new Cuenta[8];
  
        for(int i=0 ; i < 8; i++ ){
    
        cuentas[i] = new Cuenta(i);
    
    }
        
     Scanner id = new Scanner(System.in);
     System.out.println("Ingrese el ID");
     int opcion1 = id.nextInt();
     boolean out = false;
     
     if (opcion1 < 0 || opcion1 > 8) {
     
         System.out.println("Ingrese un numero de  ID valido");
     }
     
     else{
         
     while(!out){
            
           System.out.println("1. Ver balance actual");
           System.out.println("2. Retirar Dinero");
           System.out.println("3. Despositar Dinero");
           System.out.println("4. Salir");
           
           System.out.println("Escribe una de las opciones");
           Scanner opt = new Scanner(System.in);
           int opcion2 = opt.nextInt();
            
           switch(opcion2){
               
               case 1:
                   System.out.println("Balance actual: " + cuentas[opcion1].getBalance());
                   break;
                   
               case 2:
                   System.out.println("Ingrese cantidad a retirar:");
                   Scanner plata = new Scanner(System.in);
                   float monto = plata.nextFloat();
                   cuentas[opcion1].retirarDinero(monto);
                   break;
                   
                case 3:
                   System.out.println("Ingrese cantidad a dep[ositar:");
                   Scanner plata1 = new Scanner(System.in);
                   float monto1 = plata1.nextFloat();
                   cuentas[opcion1].depositarDinero(monto1);
                   break;
                   
                case 4:
                   out=true;
                   break;
                   
                default:
                   System.out.println("Solo números entre 1 y 4");
           }

     }
        
        
    }
    
    }
}
