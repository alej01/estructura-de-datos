//Alejandro Duran B.
//2019024258

package laboratorio1;

public class MainPrueba {


    public static void main(String[] args) {
       
        Cuenta cuenta1 = new Cuenta(1122, 500.000, 4.5);
        cuenta1.depositarDinero(150.000);
        cuenta1.retirarDinero(200.000);
        
        System.out.println("Balance: "+cuenta1.getBalance()+
                " Interes Mensual: "+cuenta1.calcularInteresMensual()+" Fecha: "
                + cuenta1.getFechaCreacion());
        
        
       Cuenta cuenta2 = new Cuenta(1113, 400.000, 5.5);
       
        System.out.println("Balance: " + cuenta2.getBalance()
                           + " Interes Mensual: "+ cuenta2.calcularInteresMensual()
                           + " Fecha: " + cuenta2.getFechaCreacion());
        
    }
    
    
}
