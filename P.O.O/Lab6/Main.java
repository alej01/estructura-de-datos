/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alejandro Duran
 */
public class Main {

    public static void main(String[] args) {
        
        
        DispositivoInteligente D1 = new DispositivoInteligente("Lavadora", true);
        DispositivoInteligente D2 = new DispositivoInteligente("Lampara", true);
        DispositivoInteligente D3 = new DispositivoInteligente("Tele", true);
        DispositivoInteligente D4 = new DispositivoInteligente("SistemaSonido", true);
        DispositivoInteligente D5 = new DispositivoInteligente("Computadora", true);
        DispositivoInteligente D6 = new DispositivoInteligente("Cocina", true);
        Cuarto C1 = new Cuarto("Cuarto De Pilas");       
        Cuarto C2 = new Cuarto("Oficina"); 
        Cuarto C3 = new Cuarto("Cocina"); 
        Cuarto C4 = new Cuarto("Sala"); 
         
        C1.adDispositivo(D1);
        C1.adDispositivo(D2);
        C1.Encender();
        
        C2.adDispositivo(D2);
        C2.adDispositivo(D5);
        C2.Encender();

        C3.adDispositivo(D2);
        C3.adDispositivo(D6);
        C3.Encender();
           
        C4.adDispositivo(D2);
        C4.adDispositivo(D3);
        C4.adDispositivo(D4);
        C4.Encender();

        System.out.println("Nombre del cuarto: " + C1.getName()
                            + "\nStatus: " + C1.Encender());
       
        for(int i = 0; i < C1.getDispositivosSize() ; i++){
              DispositivoInteligente device = new DispositivoInteligente();
              device = C1.returndispo(i);
              
              System.out.println(device.getName());
            
       }
 }
}
    

