/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alejandro Duran
 */
public class DispositivoInteligente implements Controlador{
    
    private String name;
    private boolean encendido;

    public DispositivoInteligente() {
    }

    
    public DispositivoInteligente(String name, boolean encendido) {
        this.name = name;
        this.encendido = encendido;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }

    @Override
    public String Encender() {
        return "El dispositivo esta encendido";
    }

    @Override
    public String Apagar() {
        return "El dispositivo esta apagado";
    }

   

    
}
