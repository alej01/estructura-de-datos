
import java.util.ArrayList;


/**
 *
 * @author Alejandro Duran
 */
public class Cuarto implements Controlador {
    

    private ArrayList<DispositivoInteligente> dispositivos = new ArrayList<>();
    private String name;
    
    
    public Cuarto() {
    }

    public Cuarto(String name) {
        this.name = name;
    }
    
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
    public int getDispositivosSize(){
    
        return this.dispositivos.size();
    }
    public DispositivoInteligente returndispo(int key){
        
        return this.dispositivos.get(key);
    }
      
    public void adDispositivo(DispositivoInteligente device){
        this.dispositivos.add(device);
    }

   @Override
    public String Encender() {
        return "El dispositivo esta encendido";
    }

    @Override
    public String Apagar() {
        return "El dispositivo esta apagado";
    }

    
    
  
    

  
    
   
    
    

}