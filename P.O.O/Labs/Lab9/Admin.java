/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab9;

/**
 *
 * @author Alejandro Duran
 */
public class Admin {
    
    private String nombre;
    private int Clave;

    public Admin() {
    }

    public Admin(String nombre, int Clave) {
        this.nombre = nombre;
        this.Clave = Clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getClave() {
        return Clave;
    }

    public void setClave(int Clave) {
        this.Clave = Clave;
    }
    
    public void actualizar(){

    } 
    
    
    
}
