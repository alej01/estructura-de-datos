package lab9;

import java.time.LocalDateTime;
/**
 *
 * @author Alejandro Duran
 */
public class Asignacion {
    
   private String nombre;
   private boolean EsRegular;
   private LocalDateTime HoraFechaInicio;
   private LocalDateTime HoraFechaFin;
   
    public Asignacion(){}

    public Asignacion(String nombre, boolean EsRegular, LocalDateTime HoraFechaInicio) {
        this.nombre = nombre;
        this.EsRegular = EsRegular;
        this.HoraFechaInicio = HoraFechaInicio;
        this.HoraFechaFin = HoraFechaInicio.plusHours(1) ;
    }
   
    public LocalDateTime getHoraFechaInicio() {
        return HoraFechaInicio;
    }

    public void setHoraFechaInicio(LocalDateTime HoraFechaInicio) {
        this.HoraFechaInicio = HoraFechaInicio;
    }

    public LocalDateTime getHoraFechaFin() {
        return HoraFechaFin;
    }

    public void setHoraFechaFin(LocalDateTime HoraFechaFin) {
        this.HoraFechaFin = HoraFechaFin;
    }
    

   public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEsRegular() {
        return EsRegular;
    }

    public void setEsRegular(boolean EsRegular) {
        this.EsRegular = EsRegular;
    }
   
   
   
}
