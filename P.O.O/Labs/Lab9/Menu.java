
package lab9;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 *
 * @author Alejandro Duran
 */
public class Menu {
    
    public Menu() throws IOException, InterruptedException{
        
        BaseDeDatos bs = new BaseDeDatos();
        Admin admin = new Admin("fernando",4876);
        Asignacion asignacion1 = new Asignacion("Patrullar Parque", true, LocalDateTime.now());
        Asignacion asignacion2 = new Asignacion("Patrullar Escuela", true, LocalDateTime.now());
        Oficial oficial1 = new Oficial("Wil", 1235, asignacion1);
        Oficial oficial2 = new Oficial("Wil", 1234, asignacion2);
        bs.addAdmin(admin);
        bs.addOficial(oficial1);
        bs.addOficial(oficial2);
        int option;
        int option2;
        int option3;
        boolean run = true;
    while(run == true){
            
            clearScreen();
            System.out.println("\n====Menú====");
            System.out.println("1.Admin");
            System.out.println("2.Oficial");
            System.out.println("3.Salir");
          
            System.out.print("Ingrese su selección: ");
            Scanner sn = new Scanner(System.in);
            option = sn.nextInt();

            switch(option){
                case 1:
                    
                    System.err.println("\nIngrese su clave: ");
                    int clave = sn.nextInt();
                    
                    if(bs.ValidateAdminID(clave) == true){
                        
                         clearScreen();
                         System.out.println("\n====Menú====");
                         System.out.println("1.Mostrat asignacion de oficial");
                         System.out.println("2.Agregar Asignacion");
                         System.out.println("3.Editar asignacion");
                         System.out.println("4.Salir");
          
                        System.out.print("Ingrese su selección: ");
                        option2 = sn.nextInt();
                     

               switch(option2){
                case 1:
                System.out.println("\nIngrese su clave: ");
                    int key = sn.nextInt();
                    
                    if(bs.ValidateAdminID(key) == true){
                        
                        Admin newadmin = bs.returnByKey(key);
                        System.out.println("\nIngrese el id del oficial el cual desea saber su asignación: ");
                        int id = sn.nextInt();
                        Oficial oficiall = bs.returnById(id);
                        Asignacion asignacion = oficiall.getNuevaAsignacion();
                        
                        clearScreen();
                        System.out.println("\nInformacion de asignacion actual: ");
                        System.out.println("\nNombre: " + asignacion.getNombre());
                        System.out.println("\nAsignacion Regular: " + asignacion.isEsRegular());
                        System.out.println("\nHora y fecha de Inicio: " + asignacion.getHoraFechaInicio());
                        System.out.println("\nHora y fehca de Fin: " + asignacion.getHoraFechaFin());                                                             
                      } else{ 
                        
                        System.out.println("\nUsted no posee los credenciales para ingresar a la terminal.");
                    }                              
                    break;

                     case 2:
                        
                        Asignacion nuevaAsignacion = new Asignacion();
                        System.out.println("\nIngrese el nombre de la asignacion: ");
                        sn.nextLine();
                        String name = sn.nextLine();
                        nuevaAsignacion.setNombre(name);
                        System.out.println("\nIngrese si la asginacion es regular (si)(no) ");
                        String regular = sn.nextLine();
                        if(regular.equals("si") || regular.equals("Si")){
                        nuevaAsignacion.setEsRegular(true);
                        }else {nuevaAsignacion.setEsRegular(false);}
                        System.out.println("\nIngrese la hora y fecha (yyyy-MM-dd HH:mm) ");
                        DateTimeFormatter date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                        LocalDateTime startDateTime = LocalDateTime.parse(sn.nextLine(), date);
                        nuevaAsignacion.setHoraFechaInicio(startDateTime);
                        bs.addAsignacion(nuevaAsignacion);
                        break;


                    case 3:
                        
                        System.out.println("\nIngrese el id del oficial el cual desea administrar su asignación: ");
                        int id = sn.nextInt();
                        Oficial oficial = bs.returnById(id);
                        Asignacion asignacion = oficial.getNuevaAsignacion();
                        
                   
                                                 
                        System.out.println("\nIngrese el nombre de la asignacion: ");
                        String nombre = sn.nextLine();
                        asignacion.setNombre(nombre);
                        System.out.println("\nIngrese si la asginacion es regular (si)(no) ");
                        String esregular = sn.nextLine();
                        if(esregular.equals("si") || esregular.equals("Si")){
                        asignacion.setEsRegular(true);
                        }else {asignacion.setEsRegular(false);}
                        System.out.println("\nIngrese la hora y fecha (yyyy-MM-dd HH:mm) ");
                        DateTimeFormatter fecha = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                        sn.nextLine();
                        LocalDateTime fechaini = LocalDateTime.parse(sn.nextLine(), fecha);
                        asignacion.setHoraFechaInicio(fechaini);
                        bs.addAsignacion(asignacion);
                        break;
                    
                    case 4:
                    break;
                    
                    
            default:
            System.out.println("La opción "+ option +" es invalida!");
               break;

            }
                        
                    } else{ 
                        
                        System.out.println("\nUsted no posee los credenciales para ingresar a la terminal.");
                    }                            
                    break;

                case 2:
                    System.out.println("\nIngrese su ID:");
                    int ID = sn.nextInt();
                    if(bs.ValidateOficialID(ID) == true){
                        
                    clearScreen();       
                    System.out.println("\n====Menú====");
                    System.out.println("Bienvenido");
                    System.out.println("1.Mostrar Asignacion");
                    System.out.println("2.Salir");
          
                    System.out.print("Ingrese su selección: ");
                    option3 = sn.nextInt();
   

        switch(option3){
            case 1:
                System.out.println("\nIngrese su ID:");
                    int oficialid = sn.nextInt();
                    if(bs.ValidateOficialID(oficialid) == true){
                        Oficial oficial = bs.returnById(ID);
                        Asignacion asignacion = oficial.getNuevaAsignacion();
                        clearScreen();
                        System.out.println("\n===Asignacion===");
                        System.out.println("\nBienvenido " + oficial.getNombre() +" esta es la informacion de su asignación");
                        System.out.println("\nNombre:" + asignacion.getNombre());
                        System.out.println("\nAsignacion Regular: " + asignacion.isEsRegular());
                        System.out.println("\nHora y fecha de Inicio: " + asignacion.getHoraFechaInicio());
                        System.out.println("\nHora y fecha de Fin: " + asignacion.getHoraFechaFin());
                        
                    } else{ 
                        
                        System.out.println("\nUsted no posee los credenciales para ingresar a la terminal.");
                    }                              
                break;

            case 2:
                break;
                
            default:
            System.out.println("La opción "+ option +" es invalida!");
               break;

            }
                    } else{ 
                        
                        System.out.println("\nUsted no posee los credenciales para ingresar a la terminal.");
                    }                              
                    break;


                case 3:
                     run = false;
                     break;
                    
                     
                default:
                    System.out.println("La opción "+ option +" es invalida!");
                    break;

            }
        
        }

       
    }
    
     public static void clearScreen() throws IOException, InterruptedException {
        new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        System.out.print("\033[H\033[2J");
        System.out.flush();
        }
    
    }
    

