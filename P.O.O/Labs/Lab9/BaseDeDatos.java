
package lab9;

import java.util.ArrayList;

/**
 *
 * @author Alejandro Duran
 */
public class BaseDeDatos {
    
    private ArrayList<Admin> admins = new ArrayList<>();
    private ArrayList<Oficial> oficiales = new ArrayList<>();
    private ArrayList<Asignacion> asignaciones = new ArrayList<>();

   public void addAdmin(Admin newAdmin){
   
       this.admins.add(newAdmin);
   }
    
    public void addOficial(Oficial newOficial){
    
       this.oficiales.add(newOficial);
    }
    
    public void addAsignacion(Asignacion newAsignacion){
    
        this.asignaciones.add(newAsignacion);
        
    }
    
    public boolean ValidateAdminID(int id){
        
        for(int i=0; i<admins.size();i++){
        if(admins.get(i).getClave() == id){
           return true; 
        }
        }
        return false;   
        
    }
    
     public boolean ValidateOficialID(int id){
        
        for(int i=0; i<oficiales.size();i++){
        if(oficiales.get(i).getId() == id){
           return true; 
        }
        }
        return false;   
        
    }
     
    public Oficial returnById(int id){
        
        for(int i=0; i<oficiales.size();i++){
        if(oficiales.get(i).getId() == id) return oficiales.get(i);
       }
        
    return null;
   }
    
    public Admin returnByKey(int key){
        
        for(int i=0; i<admins.size();i++){
        if(admins.get(i).getClave() == key) return admins.get(i);
       }
        
    return null;
   }
}