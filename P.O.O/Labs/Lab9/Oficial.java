package lab9;

/**
 *
 * @author Alejandro Duran
 */
public class Oficial {
    
    private String nombre;
    private int id;
    private Asignacion NuevaAsignacion;

    public Oficial() {
    }

    public Oficial(String nombre, int id, Asignacion NuevaAsignacion) {
        this.nombre = nombre;
        this.id = id;
        this.NuevaAsignacion = NuevaAsignacion;
    }
    
   
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Asignacion getNuevaAsignacion() {
        return NuevaAsignacion;
    }
   
    public void setasignacion(Asignacion nuevaAsignacion){
        this.NuevaAsignacion = nuevaAsignacion;
     
}    
    
    @Override
    public String toString() {
        return "Oficial{" + "nombre=" + nombre + ", id=" + id + '}';
    }
    
    
}
